; *** Must run find_bias.pro first! ***

fddir = 'data/Science/Crisp-T/'
camid = 'camXXV'
stuff = '00119.im.ex'
pf    = '.6302.6301'
lc    = 'lc1'

science = file_search(fddir + camid + "." + stuff + pf + "_*" + lc + "*")
N4 = size(science)
N4 = N4[1]

fflSplit = strarr(N4,9)
offsets = dblarr(N4)

for i = 0,N4-1 do begin
    temp = strsplit(science[i], ".", /extract)
    temp = strsplit(temp[5], "_", /extract) ; extract offsets from science files
    offsets[i] = temp[1]
endfor

index_sorted = sort(offsets)
science_sorted = strarr(N4)
offsets_sorted = dblarr(N4)

for i = 0,N4-1 do begin
    science_sorted[i] = science[index_sorted[i]] ; sort science files
    offsets_sorted[i] = offsets[index_sorted[i]]
endfor

cube = dblarr(N,nx,ny)

for i = 0,N-1 do begin
    ii = 4 * i ; only need every 4th
    fzread, science, science_sorted[ii], h ; read science files
    science_corrected = (science - dark) / flat[i,*,*] ; correct for noise
    cube[i,*,*] = science_corrected
endfor

save, cube, filename="crisp_scan_fe6301.idl", /verbose

; Display example of corrected image:
science_example = dblarr(nx,ny)
science_example[*,*] = cube[11,*,*]
window, xsize=nx, ysize=ny
tvscl, hist_equal(congrid(science_example, nx,ny))

end

