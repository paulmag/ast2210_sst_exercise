lam2 = lam0 + 460. * 1e-4
f2   = c0 / lam2
v2   = (f2 - f0) / f0 * c0

print, "Velocity in area of second dip:", v2

x_mid  = 550
x_span = 30
y_mid  = 803
y_span = 50

v_spot = mean(map_delta_v[x_mid-x_span:x_mid+x_span , y_mid-y_span:y_mid+y_span])

print, "Avg velocity in the main sunspot:", v_spot

end
