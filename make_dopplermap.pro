map = dblarr(nx, ny)

offsets_crop = offsets[4:-5]
x  = findgen(1501)-500 ; linspace(-500, 1000, 1501)
x2 = x^2

for i = 0,nx-1 do begin
    for j = 0,ny-1 do begin
        pol_2 = poly_fit(offsets_crop, cube[4:-5, i, j], 2)
        temp = min(pol_2[0] + pol_2[1] * x + pol_2[2] * x2, x_min)
        map[i,j] = x(x_min)
    endfor
endfor

save, map, filename="dopplermap_offset.idl", /verbose ; offset dopplermap [1e-13m]
;restore, "dopplermap_offset.idl", /verbose

; Scalar sizes:
c0 = 299792458. ; [m/s]
lam0 = 630.1    ; [nm]
f0 = c0 / lam0  ; [GHz]

; Doppler formula to find velocities:
map_lam = lam0 + map * 1e-4 ; [nm]
map_f   = c0 / map_lam      ; [GHz]
map_delta_v = (map_f - f0) / f0 * c0 ; [m/s] ; dopplermap in radial velocities

print, "v_min =", min(map_delta_v), " m/s  (black)"
print, "v_max =", max(map_delta_v), " m/s  (white)"

window, xsize=nx, ysize=ny
tvscl, hist_equal(congrid(map_delta_v, nx,ny))

end

