spectrum = dblarr(N)

for i = 0,N-1 do begin
    spectrum[i] = mean(cube[i,*,*]) ; average intensity from each offset
endfor

plot, offsets, spectrum, title="Spectrum from sunspot", xtitle="offset [mÅ]", ytitle="intensity", charsize=1.8

end

