fddir = 'data/flats+darks/'
camid = 'camXXV'
date  = '2011.05.06'
pf    = '.6302'
lc    = 'lc1'

ffl = file_search(fddir + camid + "." + date + pf + ".*" + lc + "*")
N = size(ffl)
N = N[1]

nx = 1024
ny = 1024

fflSplit = strarr(N,8)
for i = 0,N-1 do begin
    temp = strsplit(ffl[i], ".", /extract)
    for j = 0,7 do begin
        fflSplit[i,j] = temp[j] ; matrix of splitted strings
    endfor
endfor

offsets = dblarr(N)
for i = 0,N-1 do begin
    offsets[i] = fflSplit[i,5] ; extract offsets from the filenames
endfor

index_sorted   = sort(offsets)
ffl_sorted     = strarr(N)
offsets_sorted = dblarr(N)

for i = 0,N-1 do begin
    ffl_sorted[i]     = ffl[index_sorted[i]]     ; sort filenames
    offsets_sorted[i] = offsets[index_sorted[i]] ; sort offsets
endfor

flat = dblarr(N,nx,ny)
for i = 0,N-1 do begin
    fzread, temp, ffl_sorted[i], h  ; read the flatfield files
    flat[i,*,*] = temp / mean(temp) ; normalize and store in cube
endfor

fzread, dark, fddir + "camXXV.2011.05.06.dark", dark_h ; read darkframe
print, dark_h

offsets = offsets_sorted
save, offsets, filename="offsets.idl", /verbose

end

